const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const dotenv = require("dotenv").config();

// Routes

const userRoutes = require("./routes/userRoutes");
const courseRoutes = require ("./routes/courseRoutes");



// Connect Express

const app = express();
const port = 3010;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));



//Mongoose Connection
mongoose.connect(`mongodb+srv://iandealday:${process.env.PASSWORD}@cluster0.qmugmsf.mongodb.net/s37-41-course-booking-api?retryWrites=true&w=majority`,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }

);


let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("Connected to MongoDB."))



app.use("/users", userRoutes);
app.use("/courses", courseRoutes);


// Server port

app.listen(port, () => console.log(`API is now online on port ${port}`));
