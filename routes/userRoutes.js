const express = require("express");
const router = express.Router();
const auth = require("../auth");

// Import
const userController = require("../controllers/userController")

// Check Email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
});

// Route for registration

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
});


// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
})



// [Activity 2]

// Route for retrieving user details
// router.post("/details", (req, res) => {

// 	// Provides the user's ID for the getProfile controller method
// 	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));

// });
router.get("/details", auth.verify, (req, res) => {
	// Provides the user's ID for the getProfile controller method

	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	console.log(req.headers.authorization)

	userController.getProfile({ id: userData.id }).then(resultFromController => res.send(resultFromController))
});




// Route for enrolling an authenticated user

router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		// user ID will be retrieved from the request header
		userId: auth.decode(req.headers.authorization).id,

		// Course ID will be retrieve from the request body
		courseId: req.body.courseId

	}

	userController.enroll(data).then(result => res.send(result));

})

// Route for get all users details

router.get("/allUsers", (req, res) => {
	userController.getAllUsersDetails(req.body).then(result => res.send(result))
});

module.exports = router;