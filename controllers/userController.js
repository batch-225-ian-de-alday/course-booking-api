const User = require("../models/user");
const Course = require("../models/course");
const bcrypt = require("bcrypt");
const auth = require("../auth");





module.exports.checkEmailExists = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

		// the find method returns a record if a match is found

		if (result.length > 1) {

			return {
				message: "Duplicate User"
			} 

		} else if (result.length > 0){

			return {
				message: "User was found"
			}

		} else {

			return {
				message: "User not found"
			}
		}

		
	})
}

module.exports.registerUser = (reqBody) => {


	let newUser = new User({

		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10) 
	})


	return newUser.save().then((user, error) => {
		if (error) {

			return false;

		} else {

			return user
		};
	});
}; 



// User authentication

/*
	Steps:
	1. Check the database if the user email exists
	2. Compare the password provided in the login form with the password stored in the database
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/


module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null ){

			return {
				message: "Not found in our database"
			}

		} else {

			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);


			if (isPasswordCorrect) {

				return {access : auth.createAccessToken(result)}

			} else {

				// if password doesn't match
				return {
					message: "password was incorrect"
				}
			};

		};

	});
};





// =====================================================

// [Activity 2]

// module.exports.getProfile = (data) => {

// 	return User.findById(data.userId).then(result => {

// 		// Changes the value of the user's password to an empty string when returned to the frontend
// 		// Not doing so will expose the user's password which will also not be needed in other parts of our application
// 		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
// 		result.password = "";

// 		// Returns the user information with the password as an empty string
// 		return result;

// 	});

// };

// Retrieve User Details
module.exports.getProfile = (userData) => {
	return User.findOne({id : userData.id}).then(result => {
		if (result == null){
			return false
		} else {
			result.password = ""
			return result
		}
	});
};


module.exports.enroll = async (data) => {


	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId : data.courseId});



		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true
			};
		});
	});

	// Add the user ID in the enrollees array of the course
	// Using the "await" keyword will allow the enroll method to complete updating the course before returning a response back to the frontend
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		// Adds the userId in the course's enrollees array
		course.enrollees.push({userId : data.userId});

		// Saves the updated course information in the database
		return course.save().then((course, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});

	});


	// Condition that will check if the user and course documents have been updated
	// User enrollment successful
	if(isUserUpdated && isCourseUpdated){
		return {
			message: "You're enrolled"
		}
	// User enrollment failure
	} else {
		return false;
	};


}


// for getting all users details only firstName and lastName only will be shown
module.exports.getAllUsersDetails = () => {
    return User.find({}).then(result => {
        
        // This will only return the firstname and lastname of all users
        new_result = result.map(user => {
            return (({ firstName, lastName }) => ({ firstName, lastName }))(user);
        })
        return new_result;
    })
};









